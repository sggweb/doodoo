module.exports = class extends doodoo.Controller {
    async isCustomAuth() {
        const Token = this.query.Token || this.get("Token");
        if (!Token) {
            this.status = 401;
            this.fail("授权失败", "Custom Unauthorized");
            return false;
        }

        try {
            const decoded = this.jwtVerify(Token);
            const custom = await this.model("custom")
                .query({ where: { id: decoded.id } })
                .fetch();
            if (!custom) {
                this.status = 401;
                this.fail("用户未授权", "Custom Unauthorized");
                return false;
            }
            if (!custom.status) {
                this.status = 401;
                this.fail("用户被禁用", "Custom Status Unauthorized");
                return false;
            }

            return (this.state.custom = custom);
        } catch (err) {
            console.log(err);
            this.status = 401;
            this.fail("授权失败", "Custom Unauthorized");
            return false;
        }
    }

    async isAppAuth() {
        const AppToken = this.query.AppToken || this.get("AppToken");
        if (!AppToken) {
            this.status = 401;
            this.fail("授权失败", "App Unauthorized");
            return false;
        }

        try {
            const decoded = this.jwtVerify(AppToken);
            const app = await this.model("app")
                .query({
                    where: {
                        id: decoded.id
                    }
                })
                .fetch();
            if (!app) {
                this.status = 401;
                this.fail("应用不存在", "App Unauthorized");
                return false;
            }

            if (!app.status) {
                this.status = 401;
                this.fail("应用被禁用", "App Status Unauthorized");
                return false;
            }
            return (this.state.app = app);
        } catch (err) {
            this.status = 401;
            this.fail("授权失败", "App Unauthorized");
            return false;
        }
    }

    async isWxaAuth() {
        try {
            let app = this.state.app;
            if (!app) {
                app = await this.isAppAuth();
            }
            if (!app) {
                this.status = 401;
                this.fail("授权失败", "App Unauthorized");
                return;
            }
            const wxa = await this.model("wxa")
                .query({
                    where: {
                        app_id: app.id
                    }
                })
                .fetch();
            if (!wxa) {
                this.status = 200;
                this.fail("小程序未授权", "Wxa Unauthorized");
                return false;
            }
            if (!wxa.authorized) {
                this.status = 200;
                this.fail("小程序已取消授权", "Wxa Has Been Unauthorized");
                return false;
            }
            if (!wxa.status) {
                this.status = 200;
                this.fail("小程序被禁用", "Wxa Status Unauthorized");
                return false;
            }

            return (this.state.wxa = wxa);
        } catch (err) {
            this.status = 200;
            this.fail("授权失败", "Wxa Unauthorized");
            return false;
        }
    }

    async isShopAuth() {
        let app = this.state.app;
        if (!app) {
            app = await this.isAppAuth();
        }
        if (!app) {
            this.status = 401;
            this.fail("授权失败", "App Unauthorized");
            return;
        }

        const appId = this.state.app.id;
        const shop = await this.model("shop")
            .query({
                where: {
                    app_id: appId
                }
            })
            .fetch();
        if (!shop) {
            this.status = 401;
            this.fail("店铺不存在", "Shop Unauthorized");
            return false;
        }

        return (this.state.shop = shop);
    }
};
