const product = doodoo.bookshelf.Model.extend({
    tableName: "shop_product",
    hasTimestamps: true
});

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "analysis_product_effect_summary",
    hasTimestamps: true,
    product: function() {
        return this.belongsTo(product, "product_id");
    }
});
