const wxa = require("./wxa");
const template = require("./template");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "app",
    hasTimestamps: true,
    wxa: function() {
        return this.hasOne(wxa, "app_id");
    },
    template: function() {
        return this.belongsTo(template, "template_id");
    }
});
