const wxa = require("./wxa");
const template_wxa = require("./template_wxa");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "wxa_audit",
    hasTimestamps: true,
    wxa: function() {
        return this.belongsTo(wxa, "wxa_id");
    },
    template_wxa: function() {
        return this.belongsTo(template_wxa, "template_wxa_id");
    }
});
