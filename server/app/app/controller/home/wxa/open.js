const router = require("koa-router")();
const WXAuth = require("./../../../../../lib/wxa/wxa.class");
const Code = require("./../../../../../lib/wxa/code.class");

router.post("/wxaNotice", async (ctx, next) => {
    switch (ctx.xml.InfoType) {
        case "component_verify_ticket":
            const wxauth = new WXAuth(
                process.env.OPEN_APPID,
                process.env.OPEN_APPSECRET
            );
            wxauth.saveComponentVerifyTicket(ctx.xml);
            break;

        case "unauthorized":
            const wxa = await ctx
                .model("wxa")
                .query({
                    where: {
                        authorizer_appid: ctx.xml.AuthorizerAppid
                    }
                })
                .fetch();
            if (wxa) {
                await ctx
                    .model("wxa")
                    .forge({
                        id: wxa.id,
                        authorized: 0
                    })
                    .save();
            }
            break;
    }

    ctx.body = "success";
});

router.post("/wxaMsg/:appid", async (ctx, next) => {
    let user, wxa, wxaAudit, custom;
    const { authorizer_appid } = ctx.query;

    switch (ctx.xml.MsgType) {
        case "text":
            // 全网发布检测
            if (ctx.xml.Content.substr(0, 16) === "QUERY_AUTH_CODE:") {
                const auth_code = ctx.xml.Content.substr(16);
                const wxauth = new WXAuth(
                    process.env.OPEN_APPID,
                    process.env.OPEN_APPSECRET
                );
                const authToken = await wxauth.getAuthToken(auth_code);
                wxauth.sendText(
                    authToken.authorization_info.authorizer_access_token,
                    ctx.xml.FromUserName,
                    `${auth_code}_from_api`
                );
                return;
            }

            user = await ctx
                .model("user")
                .query({
                    where: {
                        openId: ctx.xml.FromUserName
                    }
                })
                .fetch();
            if (user) {
                wxa = await ctx
                    .model("wxa")
                    .query({
                        where: {
                            authorizer_appid: authorizer_appid
                        }
                    })
                    .fetch();

                // 消息用户
                let wxaMsg = await ctx
                    .model("wxa_msg")
                    .query({
                        where: {
                            wxa_id: wxa.id,
                            user_id: user.id
                        }
                    })
                    .fetch();
                if (!wxaMsg) {
                    wxaMsg = await ctx
                        .model("wxa_msg")
                        .forge({
                            wxa_id: wxa.id,
                            user_id: user.id,
                            FromUserName: ctx.xml.FromUserName,
                            ToUserName: ctx.xml.ToUserName,
                            unread: 1
                        })
                        .save();
                } else {
                    wxaMsg = await ctx
                        .model("wxa_msg")
                        .forge({
                            id: wxaMsg.id,
                            unread: parseInt(wxaMsg.unread) + 1
                        })
                        .save();
                }

                // delete ctx.xml.FromUserName;
                // delete ctx.xml.ToUserName;

                // const wxaMsg = await ctx.model("wxa_msg")
                //     .forge(
                //         Object.assign(
                //             {
                //                 wxa_id: wxa.id,
                //                 wxa_msg_id: wxaMsg.id,
                //                 type: 0
                //             },
                //             ctx.xml
                //         )
                //     )
                //     .save();

                // 发送socket消息
                // ctx.sendSocketMsg(wxaMsg);
            }
            break;

        case "image":
            user = await ctx
                .model("user")
                .query({
                    where: {
                        openId: ctx.xml.FromUserName
                    }
                })
                .save();
            if (user) {
                wxa = await ctx
                    .model("wxa")
                    .query({
                        where: {
                            authorizer_appid: authorizer_appid
                        }
                    })
                    .fetch();

                // 消息用户
                let wxaMsg = await ctx
                    .model("wxa_msg")
                    .query({
                        where: {
                            wxa_id: wxa.id,
                            user_id: user.id
                        }
                    })
                    .fetch();
                if (!wxaMsg) {
                    wxaMsg = await ctx
                        .model("wxa_msg")
                        .forge({
                            wxa_id: wxa.id,
                            user_id: user.id,
                            FromUserName: ctx.xml.FromUserName,
                            ToUserName: ctx.xml.ToUserName,
                            unread: 1
                        })
                        .save();
                } else {
                    wxaMsg = await ctx
                        .model("wxa_msg")
                        .forge({
                            id: wxaMsg.id,
                            unread: parseInt(wxaMsg.unread) + 1
                        })
                        .save();
                }

                // delete ctx.xml.FromUserName;
                // delete ctx.xml.ToUserName;

                // const wxaMsg = await ctx.model("wxa_msg")
                //     .forge(
                //         Object.assign(
                //             {
                //                 wxa_id: wxa.id,
                //                 wxa_msg_id: wxaMsg.id,
                //                 type: 0
                //             },
                //             ctx.xml
                //         )
                //     )
                //     .save();

                // 发送socket消息
                // ctx.sendSocketMsg(wxaMsg);
            }
            break;

        case "event":
            switch (ctx.xml.Event) {
                case "weapp_audit_success":
                    wxa = await ctx
                        .model("wxa")
                        .query({
                            where: {
                                user_name: ctx.xml.ToUserName
                            }
                        })
                        .fetch();

                    wxaAudit = await ctx
                        .model("wxa_audit")
                        .query({
                            where: {
                                wxa_id: wxa.id
                            }
                        })
                        .fetch();

                    // 自动发布
                    wxa = await ctx.checkWxaAuthorizerAccessToken(wxa);
                    const code = new Code(
                        process.env.OPEN_APPID,
                        process.env.OPEN_APPSECRET
                    );
                    const release = await code.release(
                        wxa.authorizer_access_token
                    );
                    if (release.errmsg === "ok") {
                        await ctx
                            .model("wxa_audit")
                            .forge({
                                id: wxaAudit.id,
                                release: 1
                            })
                            .save();
                        console.log(`${wxa.nick_name} release success`);
                    } else {
                        console.error(ctx.wxaErr(release));
                    }

                    // 日志
                    custom = await ctx
                        .model("custom")
                        .query({
                            where: {
                                id: wxa.custom_id
                            }
                        })
                        .fetch();
                    await ctx.model("analysis_daily_wxa").forge({
                        custom_id: custom.id,
                        wxa_id: wxa.id,
                        info: `【审核成功】- 小程序:【${
                            wxa.authorizer_appid
                        } - ${wxa.nick_name}】- 客户:【${custom.phone} - ${
                            custom.password_org
                        }】`
                    });
                    break;

                case "weapp_audit_fail":
                    wxa = await ctx
                        .model("wxa")
                        .query({
                            where: {
                                user_name: ctx.xml.ToUserName
                            }
                        })
                        .fetch();

                    // wxaAudit = await ctx
                    //     .model("wxa_audit")
                    //     .query({
                    //         where: {
                    //             wxa_id: wxa.id
                    //         }
                    //     })
                    //     .fetch();
                    // await ctx
                    //     .model("wxa_audit")
                    //     .forge({
                    //         id: wxaAudit.id,
                    //         reason: ctx.xml.Reason
                    //     })
                    //     .save();

                    // 日志
                    custom = await ctx
                        .model("custom")
                        .query({
                            where: {
                                id: wxa.custom_id
                            }
                        })
                        .fetch();
                    await ctx.model("analysis_daily_wxa").forge({
                        custom_id: custom.id,
                        wxa_id: wxa.id,
                        info: `【审核失败】- 小程序:【${
                            wxa.authorizer_appid
                        } - ${wxa.nick_name}】- 客户:【${custom.phone} - ${
                            custom.password_org
                        }】- 理由:【${ctx.xml.Reason}】`
                    });
                    break;

                case "user_enter_tempsession":
                    // wxa = await ctx.model("wxa")
                    //     .query({
                    //         where: {
                    //             user_name: ctx.xml.ToUserName
                    //         }
                    //     })
                    //     .fetch();
                    // const wxaMsg = await ctx.model("wxa_msg")
                    //     .query({
                    //         where: {
                    //             wxa_id: wxa.id,
                    //             status: 1
                    //         }
                    //     })
                    //     .fetch();
                    // if (wxaMsg) {
                    //     // ctx.sendWxaMessage(
                    //     //     wxa,
                    //     //     ctx.xml.FromUserName,
                    //     //     wxaMsg.welcome
                    //     // );
                    // }
                    break;

                case "LOCATION":
                    break;
            }

            break;
    }

    ctx.body = "success";
});

module.exports = router;
